//Por defecto los tooltips y los popovers estan desactivados, con esto los activamos
$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 5000
    })

    //el 'on' se usa en jQuery para suscribir eventos
    $("#contacto").on("show.bs.modal", function(e){
        console.log('el modal se esta mostrando');
        $("#contactoBtn").removeClass('btn-outline-success');
        $("#contactoBtn").addClass('btn-danger');
        $("#contactoBtn").prop('disabled', true);
    });
    $("#contacto").on("shown.bs.modal", function(e){
        console.log('el modal se ha mostrado');
    });
    $("#contacto").on("hide.bs.modal", function(e){
        console.log('el modal se esta ocultando');
        $("#contactoBtn").addClass('btn-outline-success');
        $("#contactoBtn").removeClass('btn-danger');
        $("#contactoBtn").prop('disabled', false);
    });
    $("#contacto").on("hidden.bs.modal", function(e){
        console.log('el modal se ha ocultado');
    });
});